<?php

namespace App\Repository;

use App\Entity\QuotationClientRow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuotationClientRow|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuotationClientRow|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuotationClientRow[]    findAll()
 * @method QuotationClientRow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuotationClientRowRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuotationClientRow::class);
    }

    // /**
    //  * @return QuotationClientRow[] Returns an array of QuotationClientRow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuotationClientRow
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
