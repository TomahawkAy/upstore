<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getProductWithMostProfit()
    {
        $manager=$this->getEntityManager();
        //select max((product.sell_price-(product.sell_price*product.tva))-(product.buy_price-
        //(product.buy_price*product.tva))),id from product group by id order by max(product.sell_price-product.buy_price) desc
        $query=$manager->createQuery('select  p from App\Entity\Product p group by p.id order by max((p.sellPrice-(p.sellPrice*p.TVA))-(p.buyPrice-
        (p.buyPrice*p.TVA))) DESC');
        $data= $query->getResult();
        return $data[0];
    }

    public function getProductWithLessProfit()
    {
        $manager=$this->getEntityManager();
        //select max((product.sell_price-(product.sell_price*product.tva))-(product.buy_price-
        //(product.buy_price*product.tva))),id from product group by id order by max(product.sell_price-product.buy_price) desc
        $query=$manager->createQuery('select  p from App\Entity\Product p group by p.id order by min((p.sellPrice-(p.sellPrice*p.TVA))-(p.buyPrice-
        (p.buyPrice*p.TVA))) asc');
        $data= $query->getResult();
        return $data[0];
    }

    public function getAverageProductProfits()
    {
        $manager=$this->getEntityManager();
        $query=$manager->createQuery('select  avg ((p.sellPrice-(p.sellPrice*p.TVA))-(p.buyPrice-
        (p.buyPrice*p.TVA))) as x from App\Entity\Product p');
        $data= $query->getResult();
        return $data[0]['x'];
    }

}
