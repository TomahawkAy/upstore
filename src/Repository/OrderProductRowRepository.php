<?php

namespace App\Repository;

use App\Entity\OrderClient;
use App\Entity\OrderProductRow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrderProductRow|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderProductRow|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderProductRow[]    findAll()
 * @method OrderProductRow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderProductRowRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrderProductRow::class);
    }

    public function countOrderItems(OrderClient $orderClient)
    {
        return $this->createQueryBuilder('R')
            ->where('R.orderRow = :order')
            ->setParameter('order',$orderClient)
            ->getQuery()
            ->getResult();
    }


    public function getSumOfOrderRows(OrderClient $orderClient)
    {
        return $this->createQueryBuilder('R')
        ->andWhere('R.orderRow = :order')
        ->setParameter('order', $orderClient)
        ->select('SUM(R.sum) as sumOfRow')
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function getTTCSumOfOrderRows(OrderClient $orderClient)
    {
        return $this->createQueryBuilder('R')
            ->andWhere('R.orderRow = :order')
            ->setParameter('order', $orderClient)
            ->select('SUM(R.TTC) as sumOfRow')
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function geOTTSumOfOrderRows(OrderClient $orderClient)
    {
        return $this->createQueryBuilder('R')
            ->andWhere('R.orderRow = :order')
            ->setParameter('order', $orderClient)
            ->select('SUM(R.OTTotal) as sumOfRow')
            ->getQuery()
            ->getSingleScalarResult();
    }



    // /**
    //  * @return OrderProductRow[] Returns an array of OrderProductRow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderProductRow
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
