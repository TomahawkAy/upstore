<?php

namespace App\Repository;

use App\Entity\ClientsGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ClientsGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientsGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientsGroup[]    findAll()
 * @method ClientsGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientsGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ClientsGroup::class);
    }

    // /**
    //  * @return ClientsGroup[] Returns an array of ClientsGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientsGroup
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
