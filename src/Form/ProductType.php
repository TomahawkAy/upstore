<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Entity\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference')
            ->add('libelle')
            ->add('quantite')
            ->add('codeBar')
            ->add('sellPrice')
            ->add('buyPrice')
            ->add('TVA')
            ->add('category',EntityType::class,array(
                'class'=>'App\Entity\Category',
                'choice_label'=>'name',
                'multiple'=>false,
                'placeholder' => 'Choisire une catégorie',
                'required'=>false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
