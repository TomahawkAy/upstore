<?php

namespace App\Form;

use App\Entity\OrderClient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use function Sodium\add;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client',EntityType::class,array(
                'class'=>'App\Entity\Client',
                'choice_label'=>'fullName',
                'multiple'=>false,
                'placeholder' => 'Choisire un client',
                'required'=>false
            ))
            ->add('isWithDelivery',CheckboxType::class,array(
                'required' => false,
                'empty_data' => false
            ));


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderClient::class,
        ]);
    }
}
