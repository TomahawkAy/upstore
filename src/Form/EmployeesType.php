<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class EmployeesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName')
            ->add('email')
            ->add('phone')
            ->add('login')
            ->add('password')
            ->add('netSalary')
            ->add('bonusSalary')
            ->add('address',EntityType::class,array(
                'class'=>'App\Entity\Address',
                'choice_label'=>'displayName',
                'multiple'=>false
            ))
            ->add('department',EntityType::class,array(
                'class'=>'App\Entity\Department',
                'choice_label'=>'name',
                'multiple'=>false
            ))
            ->add('imageProfile',FileType::class,
                array(
                    'required'=>false
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
