<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuotationRepository")
 */
class Quotation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuotationClientRow", mappedBy="quotation")
     */
    private $quotationClientRows;

    public function __construct()
    {
        $this->quotationClientRows = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?Employee
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?Employee $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|QuotationClientRow[]
     */
    public function getQuotationClientRows(): Collection
    {
        return $this->quotationClientRows;
    }

    public function addQuotationClientRow(QuotationClientRow $quotationClientRow): self
    {
        if (!$this->quotationClientRows->contains($quotationClientRow)) {
            $this->quotationClientRows[] = $quotationClientRow;
            $quotationClientRow->setQuotation($this);
        }

        return $this;
    }

    public function removeQuotationClientRow(QuotationClientRow $quotationClientRow): self
    {
        if ($this->quotationClientRows->contains($quotationClientRow)) {
            $this->quotationClientRows->removeElement($quotationClientRow);
            // set the owning side to null (unless already changed)
            if ($quotationClientRow->getQuotation() === $this) {
                $quotationClientRow->setQuotation(null);
            }
        }

        return $this;
    }
}
