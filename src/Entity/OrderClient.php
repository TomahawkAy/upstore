<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class OrderClient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="orders")
     * @ORM\JoinColumn(nullable=true)
     */
    private $client;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TTCSum;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $OTSum;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $reductionPercentage;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="orders")
     */
    private $createdBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invoice", mappedBy="invoiceOrder")
     */
    private $invoices;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderProductRow", mappedBy="orderRow")
     */
    private $orderProductRows;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWithDelivery;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isClientIncluded;


    public function __construct()
    {
        $this->invoices = new ArrayCollection();
        $this->orderProductRows = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getTTCSum(): ?float
    {
        return $this->TTCSum;
    }

    public function setTTCSum(?float $TTCSum): self
    {
        $this->TTCSum = $TTCSum;

        return $this;
    }

    public function getOTSum(): ?float
    {
        return $this->OTSum;
    }

    public function setOTSum(?float $OTSum): self
    {
        $this->OTSum = $OTSum;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getReductionPercentage(): ?float
    {
        return $this->reductionPercentage;
    }

    public function setReductionPercentage(?float $reductionPercentage): self
    {
        $this->reductionPercentage = $reductionPercentage;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedBy(): ?Employee
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?Employee $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setInvoiceOrder($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getInvoiceOrder() === $this) {
                $invoice->setInvoiceOrder(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|OrderProductRow[]
     */
    public function getOrderProductRows(): Collection
    {
        return $this->orderProductRows;
    }

    public function addOrderProductRow(OrderProductRow $orderProductRow): self
    {
        if (!$this->orderProductRows->contains($orderProductRow)) {
            $this->orderProductRows[] = $orderProductRow;
            $orderProductRow->setOrderRow($this);
        }

        return $this;
    }

    public function removeOrderProductRow(OrderProductRow $orderProductRow): self
    {
        if ($this->orderProductRows->contains($orderProductRow)) {
            $this->orderProductRows->removeElement($orderProductRow);
            // set the owning side to null (unless already changed)
            if ($orderProductRow->getOrderRow() === $this) {
                $orderProductRow->setOrderRow(null);
            }
        }

        return $this;
    }

    public function getIsWithDelivery(): ?bool
    {
        return $this->isWithDelivery;
    }

    public function setIsWithDelivery(bool $isWithDelivery): self
    {
        $this->isWithDelivery = $isWithDelivery;

        return $this;
    }

    public function getIsClientIncluded(): ?bool
    {
        return $this->isClientIncluded;
    }

    public function setIsClientIncluded(bool $isClientIncluded): self
    {
        $this->isClientIncluded = $isClientIncluded;

        return $this;
    }


}
