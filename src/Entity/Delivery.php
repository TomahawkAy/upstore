<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeliveryRepository")
 */
class Delivery
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client")
     */
    private $client;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee")
     */
    private $createdBy;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deliveryDeadline;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderClient")
     */
    private $productOrder;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDelivered;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }



    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedBy(): ?Employee
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?Employee $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getDeliveryDeadline(): ?\DateTimeInterface
    {
        return $this->deliveryDeadline;
    }

    public function setDeliveryDeadline(\DateTimeInterface $deliveryDeadline): self
    {
        $this->deliveryDeadline = $deliveryDeadline;

        return $this;
    }

    public function getProductOrder(): ?OrderClient
    {
        return $this->productOrder;
    }

    public function setProductOrder(?OrderClient $productOrder): self
    {
        $this->productOrder = $productOrder;

        return $this;
    }

    public function getIsDelivered(): ?bool
    {
        return $this->isDelivered;
    }

    public function setIsDelivered(bool $isDelivered): self
    {
        $this->isDelivered = $isDelivered;

        return $this;
    }
}
