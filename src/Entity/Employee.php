<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $fullName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="bigint")
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ratingAVG;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="createdBy")
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="createdBy")
     */
    private $produits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderClient", mappedBy="createdBy")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="target")
     */
    private $notifications;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="employees")
     */
    private $department;

    /**
     * @ORM\Column(type="float")
     */
    private $netSalary;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $bonusSalary;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles = array();

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastConnexion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmployeeImage")
     */
    private $imageProfile;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRatingAVG(): ?float
    {
        return $this->ratingAVG;
    }

    public function setRatingAVG(?float $ratingAVG): self
    {
        $this->ratingAVG = $ratingAVG;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setCreatedBy($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            if ($client->getCreatedBy() === $this) {
                $client->setCreatedBy(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Product $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
            $produit->setCreatedBy($this);
        }

        return $this;
    }

    public function removeProduit(Product $produit): self
    {
        if ($this->produits->contains($produit)) {
            $this->produits->removeElement($produit);
            // set the owning side to null (unless already changed)
            if ($produit->getCreatedBy() === $this) {
                $produit->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderClient[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(OrderClient $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCreatedBy($this);
        }

        return $this;
    }

    public function removeOrder(OrderClient $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getCreatedBy() === $this) {
                $order->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setTarget($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getTarget() === $this) {
                $notification->setTarget(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getNetSalary(): ?float
    {
        return $this->netSalary;
    }

    public function setNetSalary(float $netSalary): self
    {
        $this->netSalary = $netSalary;

        return $this;
    }

    public function getBonusSalary(): ?float
    {
        return $this->bonusSalary;
    }

    public function setBonusSalary(?float $bonusSalary): self
    {
        $this->bonusSalary = $bonusSalary;

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getRoles() : array
    {
        return $this->roles;
    }

    function addRole($role)
    {
        $this->roles[] = $role;
    }

    /**
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->login;
    }

    public function eraseCredentials()
    {
    }

    public function getLastConnexion(): ?\DateTimeInterface
    {
        return $this->lastConnexion;
    }

    public function setLastConnexion(?\DateTimeInterface $lastConnexion): self
    {
        $this->lastConnexion = $lastConnexion;

        return $this;
    }

    public function getImageProfile(): ?EmployeeImage
    {
        return $this->imageProfile;
    }

    public function setImageProfile(?EmployeeImage $imageProfile): self
    {
        $this->imageProfile = $imageProfile;

        return $this;
    }
}
