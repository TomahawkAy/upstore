<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuotationClientRowRepository")
 */
class QuotationClientRow
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quotation", inversedBy="quotationClientRows")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quotation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $desiredQuantity;

    /**
     * @ORM\Column(type="float")
     */
    private $reductionPercentage;

    /**
     * @ORM\Column(type="float")
     */
    private $sum;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuotation(): ?Quotation
    {
        return $this->quotation;
    }

    public function setQuotation(?Quotation $quotation): self
    {
        $this->quotation = $quotation;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getDesiredQuantity(): ?int
    {
        return $this->desiredQuantity;
    }

    public function setDesiredQuantity(int $desiredQuantity): self
    {
        $this->desiredQuantity = $desiredQuantity;

        return $this;
    }

    public function getReductionPercentage(): ?float
    {
        return $this->reductionPercentage;
    }

    public function setReductionPercentage(float $reductionPercentage): self
    {
        $this->reductionPercentage = $reductionPercentage;

        return $this;
    }

    public function getSum(): ?float
    {
        return $this->sum;
    }

    public function setSum(float $sum): self
    {
        $this->sum = $sum;

        return $this;
    }
}
