<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="notifications")
     */
    private $target;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderClient")
     */
    private $orderN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Reservation")
     */
    private $reservationN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quotation")
     */
    private $quotationN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promotion")
     */
    private $promotionN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $productN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Invoice")
     */
    private $invoiceN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Expenses")
     */
    private $expensesN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department")
     */
    private $departmentN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Delivery")
     */
    private $deliveryN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client")
     */
    private $clientN;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     */
    private $categoryN;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getTarget(): ?Employee
    {
        return $this->target;
    }

    public function setTarget(?Employee $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getOrderN(): ?OrderClient
    {
        return $this->orderN;
    }

    public function setOrderN(?OrderClient $orderN): self
    {
        $this->orderN = $orderN;

        return $this;
    }

    public function getReservationN(): ?Reservation
    {
        return $this->reservationN;
    }

    public function setReservationN(?Reservation $reservationN): self
    {
        $this->reservationN = $reservationN;

        return $this;
    }

    public function getQuotationN(): ?Quotation
    {
        return $this->quotationN;
    }

    public function setQuotationN(?Quotation $quotationN): self
    {
        $this->quotationN = $quotationN;

        return $this;
    }

    public function getPromotionN(): ?Promotion
    {
        return $this->promotionN;
    }

    public function setPromotionN(?Promotion $promotionN): self
    {
        $this->promotionN = $promotionN;

        return $this;
    }

    public function getProductN(): ?Product
    {
        return $this->productN;
    }

    public function setProductN(?Product $productN): self
    {
        $this->productN = $productN;

        return $this;
    }

    public function getInvoiceN(): ?Invoice
    {
        return $this->invoiceN;
    }

    public function setInvoiceN(?Invoice $invoiceN): self
    {
        $this->invoiceN = $invoiceN;

        return $this;
    }

    public function getExpensesN(): ?Expenses
    {
        return $this->expensesN;
    }

    public function setExpensesN(?Expenses $expensesN): self
    {
        $this->expensesN = $expensesN;

        return $this;
    }

    public function getDepartmentN(): ?Department
    {
        return $this->departmentN;
    }

    public function setDepartmentN(?Department $departmentN): self
    {
        $this->departmentN = $departmentN;

        return $this;
    }

    public function getDeliveryN(): ?Delivery
    {
        return $this->deliveryN;
    }

    public function setDeliveryN(?Delivery $deliveryN): self
    {
        $this->deliveryN = $deliveryN;

        return $this;
    }

    public function getClientN(): ?Client
    {
        return $this->clientN;
    }

    public function setClientN(?Client $clientN): self
    {
        $this->clientN = $clientN;

        return $this;
    }

    public function getCategoryN(): ?Category
    {
        return $this->categoryN;
    }

    public function setCategoryN(?Category $categoryN): self
    {
        $this->categoryN = $categoryN;

        return $this;
    }
}
