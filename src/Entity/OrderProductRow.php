<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderProductRowRepository")
 */
class OrderProductRow
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $desiredQuantity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $reductionPercentage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $OTTotal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TTC;

    /**
     * @ORM\Column(type="float")
     */
    private $sum;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderClient", inversedBy="orderProductRows")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderRow;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getDesiredQuantity(): ?int
    {
        return $this->desiredQuantity;
    }

    public function setDesiredQuantity(int $desiredQuantity): self
    {
        $this->desiredQuantity = $desiredQuantity;

        return $this;
    }

    public function getReductionPercentage(): ?float
    {
        return $this->reductionPercentage;
    }

    public function setReductionPercentage(?float $reductionPercentage): self
    {
        $this->reductionPercentage = $reductionPercentage;

        return $this;
    }

    public function getOTTotal(): ?float
    {
        return $this->OTTotal;
    }

    public function setOTTotal(?float $OTTotal): self
    {
        $this->OTTotal = $OTTotal;

        return $this;
    }

    public function getTTC(): ?float
    {
        return $this->TTC;
    }

    public function setTTC(?float $TTC): self
    {
        $this->TTC = $TTC;

        return $this;
    }

    public function getSum(): ?float
    {
        return $this->sum;
    }

    public function setSum(float $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getOrderRow(): ?OrderClient
    {
        return $this->orderRow;
    }

    public function setOrderRow(?OrderClient $orderRow): self
    {
        $this->orderRow = $orderRow;

        return $this;
    }
}
