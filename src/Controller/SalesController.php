<?php


namespace App\Controller;


use App\Entity\Notification;
use App\Entity\Order;
use App\Entity\OrderClient;
use App\Entity\OrderProductRow;
use App\Form\OrderType;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/Order")
 */
class SalesController extends AbstractController
{

/**
* @Route("/newOrder",name="newOrder")
 */
    public function newOrder(Request $request)
    {
        $order=new OrderClient();
        $form=$this->createForm(OrderType::class,$order);
        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $newNotification=new Notification();
            $newNotification->setTarget($this->get('security.token_storage')->getToken()->getUser());
            $newNotification->setState(false);
            $newNotification->setEvent("Order creation");
            $newNotification->setCreatedAt(new \DateTime('now'));
             $order->setTTCSum(0);
            $order->setUpdatedAt(new \DateTime('now'));
            $order->setTotal(12.0);
            $order->setReductionPercentage(0);
            $order->setOTSum(0);
            $order->setIsClientIncluded(false);
            $order->setCreatedBy($this->get('security.token_storage')->getToken()->getUser());
            $order->setState(false);
            $order->setCreatedAt(new \DateTime('now'));
            if ($form->get('isWithDelivery')->getData()!="1")
            {
                $order->setIsWithDelivery(false);
            }
            if (is_null($form->get('client')->getData()))
            {
                $order->setIsClientIncluded(false);
            }
            else {
                $order->setIsClientIncluded(true);
            }
            $manager=$this->getDoctrine()->getManager();
            $manager->persist($order);
            $manager->persist($newNotification);
            $manager->flush();
            return $this->redirectToRoute('OrderRowsAdding',array('order'=>$order->getId()));
        }
        return $this->render("order/newOrder.html.twig",array('form'=>$form->createView()));
    }



    /**
     * @Route("/addingOrderRow/{order}",name="OrderRowsAdding")
     */
    public function addingOrderRows(Request $request,$order)
    {
        $currentOrder = $this->getDoctrine()->getRepository(OrderClient::class)->find($order);
        $allOrderRows=$this->getDoctrine()->getRepository(OrderProductRow::class)->countOrderItems($currentOrder);
        $sumOfOrder=$this->getDoctrine()->getRepository(OrderProductRow::class)->getSumOfOrderRows($currentOrder);
        $ottotal=$this->getDoctrine()->getRepository(OrderProductRow::class)->geOTTSumOfOrderRows($currentOrder);
        $ttcSum=$this->getDoctrine()->getRepository(OrderProductRow::class)->getTTCSumOfOrderRows($currentOrder);
        if ($request->isXmlHttpRequest())
            {
         return new JsonResponse()   ;
    }
        else {

            return $this->render('order/new-command.html.twig',
            array(
                'client'=>$currentOrder->getClient(),
                'order'=>$currentOrder,
                'rowsNumber'=>count($allOrderRows),
                'sum'=>$sumOfOrder,
                'ttcsum'=>$ttcSum,
                'ottotal'=>$ottotal,
            ));
        }
    }

    /**
    * @Route("/myOrders",name="readOrders")
     */
    public function readOrders()
    {
    $orders=$this->getDoctrine()->getRepository(OrderClient::class)->findAll();
    return $this->render("order/my-orders.html.twig",array('orders'=>$orders));
    }






}