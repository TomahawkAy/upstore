<?php


namespace App\Controller;


use App\Entity\Address;
use App\Entity\Department;
use App\Entity\Employee;
use App\Entity\EmployeeImage;
use App\Entity\Notification;
use App\Form\EmployeesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/")
 */
class EmployeesController extends AbstractController
{
/**
 * @Route("/newEmployee",name="newEmployee")
*/
    public function addEmployee(Request $request) : ?Response
    {
        $departments=$this->getDoctrine()->getRepository(Department::class)->findAll();
        $manager=$this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest())
        {
            $file=$request->files->get('imageUpload');
            $employee=new Employee();
            if (!is_null($file)) {
                $image = new EmployeeImage();
                $image->setImageFile($file);
                $manager->persist($image);
                $employee->setImageProfile($image);
            }
            if ($request->get('display_name')!="empty")
            {
                $address=new Address();
                $address->setDisplayName($request->get('display_name'));
                $address->setTypePosition($request->get('type'));
                $address->setLongtitude((float)$request->get('lon'));
                $address->setLatitude((float)$request->get('lat'));
                $manager->persist($address);
                $employee->setAddress($address);
            }

            switch ($request->get('rolesEmp'))
            {
                case 'Vendeur':
                    $employee->addRole("ROLE_SELLER");
                    break;
                case 'Manager':
                    $employee->addRole("ROLE_MANAGER");
                    break;
                case 'Stock':
                    $employee->addRole("ROLE_SUPPLIER");
                    break;
            }
            $employee->setFullName($request->get('fullName'));
            $employee->setPhone((int)$request->get('phoneEmp'));
            $employee->setEmail($request->get('emailEmp'));
            $employee->setNetSalary((float)$request->get('salaireNetEmp'));
            $employee->setBonusSalary((float)$request->get('salaireBonusEmp'));
            $employee->setLogin($request->get('loginEmp'));
            $employee->setPassword($request->get('passwordEmp'));
            if ($request->get('deptEmp')!="empty")
            {
                $dept=$this->getDoctrine()->getRepository(Department::class)->find((int)$request->get('deptEmp'));
                $employee->setDepartment($dept);
            }
            $manager->persist($employee);
            $manager->flush();
            return new JsonResponse("Added");
        }


        else {
            return $this->render("employee/new-profile.html.twig",array('departments'=>$departments));
        }

    }

    public function updateEmployee()
    {

    }

    public function deleteEmployee()
    {

    }

    public function archiveEmployee()
    {

    }

    public function searchForEmployees(){

    }

    public function readEmployees(){
    $employees=$this->getDoctrine()->getRepository(Employee::class)->findAll();
    return $this->render("",array('employees'=>$employees));
    }



    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        $session = $request->getSession();
        $session->clear();
        //var_dump($session); die();
        //$session->remove('name');
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/", name="home")
     */

    public function home(Request $request)
    {
            if ( (!$this->isGranted('ROLE_MANAGER')) && (!$this->isGranted('ROLE_SELLER'))  && (!$this->isGranted('ROLE_SUPPLIER'))) {
                return $this->redirectToRoute('app_login');
            }
            else {
                return $this->render('home/index.html.twig');
            }
    }

    /**
     * @Route("/profile/{email}",name="renderProfile")
     */
    public function renderProfile(Request $request,$email)
    {
        $employee=$this->getDoctrine()->getRepository(Employee::class)->findOneBy(array('email'=>$email));
        $employeeLogs=$this->getDoctrine()->getRepository(Notification::class)->findBy(array('target'=>$employee));
        if ($request->isXmlHttpRequest())
        {
            return new JsonResponse(array('sake'=>'mobile service test'));
        }
        else {
            return $this->render("employee/profile.html.twig",array('employee'=>$employee,'logs'=>$employeeLogs));
        }
    }



}