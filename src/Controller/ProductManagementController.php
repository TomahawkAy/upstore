<?php


namespace App\Controller;


use App\Entity\Category;
use App\Entity\Notification;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Form\ProductType;
use phpDocumentor\Reflection\DocBlock\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Vich\UploaderBundle\Entity\File;

/**
 * @Route("/Products")
 */
class ProductManagementController extends AbstractController
{
    /**
     * @Route("/",name="readProducts")
     */
    public function readProducts()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        //fetching the product with the most profitable amount of money
        $ResultmostProfitable = $this->getDoctrine()->getRepository(Product::class)->getProductWithMostProfit();
        $resultLessProfitable = $this->getDoctrine()->getRepository(Product::class)->getProductWithLessProfit();
        $avg=$this->getDoctrine()->getRepository(Product::class)->getAverageProductProfits();
        return $this->render("product/index.html.twig", array('products' => $products,
            'categories' => $categories,
            'mostP' => $ResultmostProfitable,
            'lessP' => $resultLessProfitable,
            'avg'=>(int)$avg));
    }

    /**
     * @Route("/newProduct",name="newProduct")
     */
    public function addProduct(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            var_dump($request->files->get("imageUpload"));
            $productImage = new ProductImage();
            $productImage->setImageFile($request->files->get("imageUpload"));
            $product->setImage($productImage);
            $product->setCreatedAt(new \DateTime('now'));
            $product->setCreatedBy($this->get('security.token_storage')->getToken()->getUser());
            $manager = $this->getDoctrine()->getManager();
            $notification = new Notification();
            $notification->setCreatedAt(new \DateTime('now'));
            $notification->setEvent("L/'employée " . $this->get("security.token_storage")
                    ->getToken()->getUser()->getFullName()
                . "à ajouter un nouveau produit : " . $product->getLibelle());
            $notification->setState(false);
            $manager->persist($productImage);
            $manager->persist($product);
            $manager->flush();
            return $this->redirectToRoute('readProducts');
        }
        return $this->render("product/newProduct.html.twig", array('form' => $form->createView()));
    }

    /**
     * @Route("/searchForProduct",name="searchForProduct")
     */
    public function searchForProductByCodeBar(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(array('codeBar' => $request->request->get('barcode')));
            $serializer = new \Symfony\Component\Serializer\Serializer([new ObjectNormalizer()]);
            $responseProduct = $serializer->normalize($product, 'json', ["attributes" => ['id', 'reference',
                'libelle', 'quantite', 'codeBar', 'sellPrice', 'buyPrice', 'TVA']]);
            return new JsonResponse($responseProduct);
        } else {
            return new Response("please use ajax");
        }
    }


    /**
     * @Route("/updateProduct",name="updateProduct")
     */
    public function updateProduct(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $id = $request->request->get('id');
            $reference = $request->request->get('reference');
            $libelle = $request->request->get('libelle');
            $quantity = $request->request->get('quantity');
            $barCode = $request->request->get('barCode');
            $TVA = $request->request->get('TVA');
            $sellPrice = $request->request->get('sellPrice');
            $buyPrice = $request->request->get('buyPrice');
            $product = $this->getDoctrine()->getRepository(Product::class)->find((int)$id);
            if ($product != null) {
                $product->setReference($reference);
                $product->setLibelle($libelle);
                $product->setQuantite((int)$quantity);
                $product->setCodeBar($barCode);
                $product->setTVA((float)$TVA);
                $product->setSellPrice((float)$sellPrice);
                $product->setBuyPrice((float)$buyPrice);
                $manager = $this->getDoctrine()->getManager();
                $manager->flush();
                return new JsonResponse(array('operation' => 'success'));
            } else return new JsonResponse(array('operation' => $id));
        } else return new Response("use ajax");
    }




}